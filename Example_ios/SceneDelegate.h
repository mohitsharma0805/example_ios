//
//  SceneDelegate.h
//  Example_ios
//
//  Created by Mohit on 7/19/20.
//  Copyright © 2020 Mohit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

