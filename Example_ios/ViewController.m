//
//  ViewController.m
//  Example_ios
//
//  Created by Mohit on 7/19/20.
//  Copyright © 2020 Mohit. All rights reserved.
//

#import "ViewController.h"
#import "TextCell.h"

@interface ViewController ()

@end

@implementation ViewController {
    
    __weak IBOutlet UITextField *_txtName;
    __weak IBOutlet UITableView *_tableView;
    NSMutableArray *_arrList;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _arrList = [NSMutableArray new];
}

- (IBAction)btnSubmitClicked:(id)sender {
    [_txtName resignFirstResponder];
    if(_arrList.count > 0) {
        [_arrList removeAllObjects];
    }
    for (int i = 0; i < [_txtName.text length]; i++) {
        NSString *ch = [_txtName.text substringWithRange:NSMakeRange(i, 1)];
        [_arrList addObject:ch];
    }
    [_tableView reloadData];
}


// MARK: - Table View Methods

-(NSInteger)tableView:(UITableView *__unused)tableView numberOfRowsInSection:(NSInteger)__unused section {
    
    return _arrList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TextCell *cell = (TextCell *)[tableView dequeueReusableCellWithIdentifier:@"TextCell" forIndexPath:indexPath];
    cell.lblText.text = _arrList[indexPath.row];
    return cell;
}

@end
