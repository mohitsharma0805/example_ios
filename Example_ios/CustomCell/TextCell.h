//
//  TextCell.h
//  Example_ios
//
//  Created by Mohit on 7/19/20.
//  Copyright © 2020 Mohit. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TextCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblText;

@end

NS_ASSUME_NONNULL_END
